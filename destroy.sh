set -u
set -e
set -o pipefail

cd $(dirname $0)

# ===========================================================================
# parse options
# ===========================================================================

for i in "$@"
do
    case $i in
       -e=*|--environment=*)
         echo "I'm here"
         ENVIRONMENT="${i#*=}"
         ;;
       *)
         echo "Unknown option $i"
         ;;
     esac
done

echo "# =================================================================="
echo "destroying stack: $ENVIRONMENT"
echo "# =================================================================="


ansible-playbook -i inventory/cloudformation cloudformation-destroy.yml \
    --extra-vars "env=$ENVIRONMENT"
