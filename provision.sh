#!/bin/bash

set -u
set -e
set -o pipefail

cd $(dirname $0)

# ===========================================================================
# Default values
# ===========================================================================
PRIVATE_KEY=../dokku_key-private.pem
ENVIRONMENT=testing
APPLICATION_NAME=sample-node-app
AWS_KEY_NAME=dokku_key
DOKKU_DEPLOY_BRANCH=master

# ===========================================================================
# parse options
# ===========================================================================

for i in "$@"
do
    case $i in
       -k=*|--private-key=*)
         PRIVATE_KEY="${i#*=}"
         ;;
       -e=*|--environment=*)
         echo "I'm here"
         ENVIRONMENT="${i#*=}"
         ;;
       -a=*|--application-name=*)
         APPLICATION_NAME="${i#*=}"
         ;;
       -b=*|--deploy-branch=*)
         DOKKU_DEPLOY_BRANCH="${i#*=}"
         ;;
       *)
         echo "Unknown option $i"
         ;;
     esac
done

echo "# =================================================================="
echo "creating new stack:"
echo "environment $ENVIRONMENT"
echo "application name: $APPLICATION_NAME"
echo "deploy branch: $DOKKU_DEPLOY_BRANCH"
echo "# =================================================================="

chmod 400 "$PRIVATE_KEY"
ssh-add "$PRIVATE_KEY"
ssh-add -l

ansible-playbook -i inventory/cloudformation cloudformation-create.yml \
    --extra-vars "env=$ENVIRONMENT"

# wait a little bit so the ec2 instance is up and running
echo "waiting for EC2 instance to start up"
sleep 120  # TODO make this dynamic
ansible-playbook -i inventory/ec2.py dokku.yml             \
    --extra-vars "env=$ENVIRONMENT"                        \
    --extra-vars "dokku_app_name=$APPLICATION_NAME"        \
    --extra-vars "dokku_deploy_branch=$DOKKU_DEPLOY_BRANCH"

echo "Created/Updated:"
python get_instances.py --environment="$ENVIRONMENT"
echo "You may need to set this value in gitlab, so take a note :)"
