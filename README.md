# Dokku automation
This script creates a dokku node in AWS using cloudformation.

## Requirements

### software
- ansible > 2.2
- boto
- boto3

on Debian/ubuntu you can install the requirements executing:

```bash
sudo apt-get update
sudo apt-get install ansible python-boto python3-boto3
```

Alternatively, you can install the above dependencies using pip:

```bash
python3 -m venv venv
source venv/bin/activate
pip install ansible boto boto3
```
If you decide to use a `pip` installation, be aware that this script is not
fully tested with ansible 2.4.x so you may get some errors/warnings.


### AWS setup: IAM
Permissions: you need a AWS user that can execute cloudformation, create EC2
instances and configure a VPC. If you have any errors when running the
`./provision.sh` script, ask somebody with elevated privileges to help you to
fine tune your permissions to create your dokku environments.

Don't forget to export your keys (key_id/access_id) in a file named `.boto` in
your home directory with the following content:

```
[Credentials]
aws_access_key_id = ABC123aaaa...
aws_secret_access_key = Ab123AAakk3/...
```




### AWS setup: Key Pair
By default ansible expects a keypair called `dokku_key` (can be configured)
You need the private key part added to your ssh agent so ansible can ssh into
your dokku instance and configure it.


Now import the key with:

```shell
ssh-add <path to your pem file>
ssh-add -l
```

Important! don't commit your pem file, keep it somewhere outside your git
repository. This script expects a pem file named

## Ansible/AWS guide

for more information, check the official [ansible/aws guide](http://docs.ansible.com/ansible/latest/guide_aws.html)


# Create a dokku stack

Create a cloudformation stack with 1 ec2 instance, with dokku installed and
configured:

```bash
./provision.sh
```
This script takes some (optional) parameters:

* `--private-key=` or `-k`: path to the private key, a .pem file (defaults to: `../dokku_key-private.pem`)
* `--environment=` or `-e=`: environment name (string) can be staging, production, ...  (defaults to `testing`)
* `--application-name` or `-a`: specifies the dokku application name (defaults to `sample-node-app`)
* `--deploy-branch=` or `-b`: tells dokku what branch to track (defaults to `master`)

## KNOWN BUG:

after the first installation, when docker and dokku get installed, you *may* need
to restart the machine, so for now, log on into your AWS console and restart
the instance (I am working on this specific issue)

### Configuration

Configuration is stored into two different files: `files/cloudformation.json`
and `group_vars/dokku/vars`

#### ssh key configuration for pushing code to dokku
NOTE: this repo stores a public and private (encrypted) key pair. Those files
will be used to push changes from gitlab to dokku
So, if you want to use your own keys, fork the project and add replace:

```
roles/dokku/files/id_rsa
roles/dokku/files/id_rsa.pub
```

Tip: you can use `ssh-keygen -f roles/dokku/files/id_rsa`
please don't commit your unencrypted private key
The private key should be used to as private variable in gitlab to trigger the


*TODO* store all the configuration into a single place exposing the ami/ports/...

## Destroy a dokku stack (debugging)

If you change your configuration and you get cloudformation errors, execute:

`./destroy.sh -e=<your environment name>`

This will completely remove your stack so be careful when calling this script.

### Example Create the production stack

If you want to create another stack, call the following script:

```shell
./provision --environment=production -a=myapp -b=release
```
This script will create the production stack (if not existing), create a dokku
application named `myapp` and set it to follow the `release` branch.


### Limitations

The default configuration defaults to `us-east-1`, `t2.micro` instance and testing
environment, be aware that the instance type/region limit your AMI choices,
if you need to update them, remember to update your `files/cloudformation.json`
template.

This specific project assumes your dokku server runs on Ubuntu 16.04 LTS,
different distributions are not tested and may not work.

All dokku instances will expect to receive changes on `master` branch


## Next steps

Automation here should be better integrated with gitlab, it could update the
environments and dokku hosts as soon there are some changes in the stack

Wrap everything into a docker container, so we can can remove the packages/pip
dependencies
