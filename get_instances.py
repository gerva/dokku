from __future__ import print_function
import argparse
import boto.rds

def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument('--region', help='name of the AWS region',
                        default='us-east-1')
    parser.add_argument('--environment', help='name of the environment',
                        default='all')
    return parser.parse_args()


def main():
    args = parse_cli()
    region = args.region
    environment = args.environment
    ec2_conn = boto.ec2.connect_to_region(region)
    for ec2 in ec2_conn.get_all_instances():
        public_dns_name = ec2.instances[0].public_dns_name
        if not public_dns_name:
            break

        try:
            stack_name = ec2.instances[0].tags['aws:cloudformation:stack-name']
        except KeyError:
            # this instance does not belong to any stack
            break
        msg = '{0} ({1})'.format(public_dns_name, stack_name.split('-')[-1])
        if environment == 'all':
            print(msg)
        elif environment in msg:
            print(msg)

if __name__ == '__main__':
    main()
